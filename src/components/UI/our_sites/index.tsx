import React from "react";
import {
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonIcon,
} from "@ionic/react";
import { d, er, i_w } from "../../../assets/svgs";
import { elexhub_url } from "../../../tools/urls";
import { logoInstagram } from "ionicons/icons";

const OurSites = () => {
  return (
    <div>
      <IonContent>
        <IonFab horizontal="end" vertical="bottom" slot="fixed">
          <IonFabButton color="dark">
            <IonIcon icon={i_w}></IonIcon>
          </IonFabButton>
          <IonFabList side="top">
            <IonFabButton href={elexhub_url} color="dark">
              <IonIcon icon={i_w}></IonIcon>
            </IonFabButton>
            <IonFabButton color="dark">
              <IonIcon icon={d}></IonIcon>
            </IonFabButton>
            <IonFabButton color="dark">
              <IonIcon icon={er}></IonIcon>
            </IonFabButton>
          </IonFabList>
        </IonFab>
      </IonContent>
    </div>
  );
};

export default OurSites;
