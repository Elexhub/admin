import {
  IonFab,
  IonIcon,
  IonTitle,
  IonFabList,
  IonContent,
  IonFabButton,
} from "@ionic/react";
import React from "react";
import { dbi, l, signin_logo, signup_logo } from "../../assets/svgs";
import "@ionic/react/css/core.css";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "../../styles/index.css";
import { Nav, Navbar, Container } from "react-bootstrap";

/**
  @author abdfnx
  @function Header
**/

const Header = () => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
      <Container>
        <Navbar.Brand href="/">
          <IonTitle>
            <img src={l} alt="admin logo" />
          </IonTitle>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>
            <IonContent fullscreen class="ion-padding">
              <IonFab horizontal="end" vertical="top" slot="fixed" edge>
                <IonFabButton color="light">
                  <IonIcon icon={dbi} />
                </IonFabButton>
                <IonFabList>
                  <IonFabButton href="/signin" color="light">
                    <IonIcon icon={signin_logo} />
                  </IonFabButton>
                  <IonFabButton href="/signup" color="light">
                    <IonIcon icon={signup_logo} />
                  </IonFabButton>
                </IonFabList>
              </IonFab>
            </IonContent>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
