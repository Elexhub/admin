import l from "./logo.svg";
import i from "./icon.svg";
import i_w from './icon-w.svg';
import signin_logo from "./signin.svg";
import signup_logo from "./signup.svg";
import d from './drov.svg';
import er from './erobot.svg';
import dbi from './db.svg';

export { l, i, signin_logo, signup_logo, i_w, d, er, dbi };
